models package
==============

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   models.cnn
   models.rcnn

Module contents
---------------

.. automodule:: models
   :members:
   :undoc-members:
   :show-inheritance:
